					<?php echo do_shortcode('[om_gmap zoom="14" lat="33.7436165" lng="-118.1028947" infowindow="<h5>Alternative Surf</h5>330 Main Street<br />Seal Beach, CA 90740" marker="/wp-content/uploads/marker.png" styles="_light_monochrome"]') ?>				

					<footer class="footer" role="contentinfo">
						<div id="inner-footer" class="row">
							<div class="large-3 medium-12 columns">
								<ul>
									<a href="/"><img src="/wp-content/uploads/footer-logo.png" /></a>
									<li class="address">330 Main St. Suite D<br />
									Seal Beach, CA 90740<br />
									<a>562.277.4719</a></li>
								</ul>
								<form role="search" method="get" class="footer-search-form" action="http://www.alternativesurf.com/">
									<label>
										<input type="search" placeholder="Search" value="" name="s" title="Search efter:" />
									</label>
									<input type="submit" class="search-submit" value="Search">
								</form>	
		    				</div>
		    				<div class="large-2 medium-12 columns">
								<div class="data-block-one">
									<h6 class="trigger">Information</h6>
			 						<ul class="data-collapse">
										<li><a href="/hours/">Hours &amp; Location</a></li>
										<li><a href="/about/">About</a></li>
										<li><a href="http://www.instagram.com/alternative_surf" target="_blank">#AlternativeSurf</a></li>
										<li><a href="/contact/">Contact</a></li>
										<li><a href="/sitemap/">Sitemap</a></li>
									</ul>
								</div>
		    				</div>
		    				<div class="large-2 medium-12 columns">
								<div class="data-block-two">
									<h6 class="trigger">Store Links</h6>
									<ul class="data-collapse">
										<li><a href="/product-category/bodyboards/">Bodyboards</a></li>
										<li><a href="/product-category/fins/">Fins</a></li>
										<li><a href="/product-category/wetsuits/">Wetsuits</a></li>
										<li><a href="/product-category/apparel/">Apparel</a></li>
										<li><a href="/product-category/accessories/">Accessories</a></li>
									</ul>
								</div>
		    				</div>
		    				<div class="large-2 medium-12 columns">
								<div class="data-block-three">
									<h6 class="trigger">My Account</h6>
									<ul class="data-collapse">									
										<li><a href="/shop/cart/">My Cart</a></li>
										<li><a href="/shop/my-account/">Account</a></li>
										<li><a href="/shop/checkout/">Checkout</a></li>
										<li><a href="/shop/shipping-returns/">Shipping/Returns</a></li>
									</ul>
		    					</div>
		    				</div>
		    				<div class="large-3 medium-12 columns">
								<ul>
									<li><h6>Join Our Newsletter</h6></li>
									<li>
										<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:3;">
											<div class="row">
											    <div class="large-12 columns">
											      <div class="row collapse">
											        <div class="small-10 columns">
											          <input name="ea" size="20" value="" type="text" class="large-8 columns">
											        </div>
											        <div class="small-2 columns">
											          <button class="footer-btn" type="submit">></button>
											        </div>
											      </div>
											    </div>
											</div>
											<input name="llr" value="rd4pwjcab" type="hidden">
											<input name="m" value="1101984693361" type="hidden">
											<input name="p" value="oi" type="hidden">
										</form>
									</li>
									<li>Stay Connected</li>
									<li><a href="http://www.facebook.com/AlternativeSurf/" target="_blank"><i class="fa fa-facebook-square"></i></a> <a href="http://www.instagram.com/alternative_surf" target="_blank"><i class="fa fa-instagram"></i></a></li>
								</ul>
		    				</div>
							<div class="large-12 medium-12 columns">
								<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> | All Rights Reserved | Site by <a href="http://www.gibbardwebdesign.com/" target="_blank">GWD</a></p>
							</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div> <!-- end #container -->
			</div> <!-- end .inner-wrap -->
		</div> <!-- end .off-canvas-wrap -->
		<?php wp_footer(); ?>
		<script type="text/javascript">
		    $(".data-block-one h6").click(function () {
		    $('.data-block-one .data-collapse').toggleClass('show');
		});		    
	    </script>
	    <script type="text/javascript">
		    $(".data-block-two h6").click(function () {
		    $('.data-block-two .data-collapse').toggleClass('show');
		});		    
	    </script>
	    <script type="text/javascript">
		    $(".data-block-three h6").click(function () {
		    $('.data-block-three .data-collapse').toggleClass('show');
	    });		
	    </script>
	</body>
</html> <!-- end page -->
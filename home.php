<?php get_header(); ?>

<?php putRevSlider("homepage","homepage") ?>
			
			<div id="content">
			
				<div id="inner-content" class="row">
			
				    <div id="main" class="large-12 medium-12 columns" role="main">
					 
					    <h3 class="subslidertext">Come See <span>What We're About</span></h3>
						<div class="large-6 medium-6 small-12 columns google-map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2sus!4v1439487979838!6m8!1m7!1s63gxu-V8qGQAAAQZHhjWBw!2m2!1d33.74365282639243!2d-118.1029875227906!3f123.94!4f0!5f0.7820865974627469" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<div class="large-6 medium-6 small-12 columns product-boxes">
							<?php echo do_shortcode('[product_categories columns="3" ids="716,249,253,252,255,269"]') ?>
						</div>
						<div class="large-12 medium-12 columns banner">
							<a href="/product-category/accessories/other-bodyboarder-needs/"><img src="/wp-content/uploads/hero4sessionbanner.jpg"></a>
						</div>
							
						<div class="large-12 medium-12 columns hottest-products">
							<h3><span>Hottest Products</span></h3>
							<ul class="hottest-categories">
								<li><a href="/product-category/bodyboards/">Bodyboards</a></li>
								<li><a href="/product-category/swim-fin-accessories/">Fins</a></li>
								<li><a href="/product-category/wetsuits/">Wetsuits</a></li>
							</ul>
							<div class="row">
								<?php echo do_shortcode('[recent_products per_page="3" columns="3"]') ?>
							</div>	
						</div>
						
				    </div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

			<div class="instagram-footer">
				<h5><span><a href=="http://www.instagram.com/alternativesurf" target="_blank">#AlternativeSurf</a></span></h5>			
				<?php echo do_shortcode('[instagram-feed id="322464276"]'); ?>
			</div>

<?php get_footer(); ?>